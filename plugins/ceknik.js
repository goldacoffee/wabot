let fetch = require('node-fetch')

let handler = async(m, { conn, args, usedPrefix, command }) => {
    if (!(args[0])) throw `Perintah Salah, Contoh:\n${usedPrefix + command} 1401031011940003`

    let res = await fetch(global.API('https://ppsdm.lkpp.go.id/api/participant/nik/' + args[0]))
    let json = await res.json()
    let mes = `${json.message}`
    m.reply(mes)

}

handler.help = ['alquran *114 1*']
handler.tags = ['quran']
handler.group = true
handler.fail = null

handler.command = /^ceknik$/i
module.exports = handler