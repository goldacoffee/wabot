let fetch = require('node-fetch')

let handler = async(m, { conn, args, usedPrefix, command }) => {
    if (!(args[0])) throw `Perintah Salah, Contoh:\n${usedPrefix + command} 197902282009121002`

    let res = await fetch(global.API('https://ppsdm.lkpp.go.id/api/participant/' + args[0]))
    let json = await res.json()
    if (json.code == '1') {
        let mes = `Akun belum terdaftar, \n\n${json.data}`
        m.reply(mes)
    } else {
        let mes = `${ json.message }\nhttps://ppsdm.lkpp.go.id/auth/login`
        m.reply(mes)

    }



}

handler.help = ['ceknip 197902282009121002']
handler.tags = ['cek']
handler.group = true
handler.fail = null
handler.command = /^ceknip$/i
module.exports = handler